
let Formulaire_OverWatch = document.forms["Form_OverWatch"];

let affichage_Played = document.getElementById("Parties_jouées");
let affichage_Classement = document.getElementById("Classement");
let affichage_Victoire = document.getElementById("Victoires");
let affichage_Deff = document.getElementById("Défaites");
let affichage_Image_Profil = document.getElementById("Image_Profil_joueur");

let var_Error;
let var_Error_Affichage = document.getElementById("ErrorAff");

Formulaire_OverWatch.addEventListener("submit", function(form_Data)
{
	form_Data.preventDefault();
	console.log(form_Data);

	let var_Inputs = this;	//document.forms["Form_OverWatch"];

	let var_Regex_Nom = new RegExp(`^[A-Z]{1}[a-z-' ]+$`, `g`);
	let var_Name_Tbl = var_Inputs["nom"];
	let var_Name_value = var_Name_Tbl.value;
	let test_Nom = var_Regex_Nom.test(var_Name_value);


	let var_Regex_Prenom = new RegExp(`^[A-Z]{1}[a-z-' ]+$`, `g`);
	let var_Prenom_Tbl = var_Inputs["prenom"];
	let var_Prenom_value = var_Prenom_Tbl.value;
	let test_Prenom = var_Regex_Prenom.test(var_Prenom_value);


	let var_BattleTag_Tbl = var_Inputs["battletag"];
	let regex_Croisillon = /#/gi;
	let var_BattleTag_value = var_BattleTag_Tbl.value.replace(regex_Croisillon, "-");


	let var_Platform_Tbl = var_Inputs["platform"];
	let var_Platform_value = var_Platform_Tbl.value;


	let var_URL_API = `https://ow-api.com/v1/stats/${var_Platform_value}/us/${var_BattleTag_value}/profile`;


	function reset_Fields(Form_Fields)
	{
		for (let i = 0; i < Form_Fields.length; i++)
		{
			Form_Fields[i].value = "";
		}
	}	

	if (test_Nom)
	{
		var_Error = "";
		var_Error_Affichage.innerHTML = var_Error;

		if (test_Prenom)
		{
			var_Error = "";
			var_Error_Affichage.innerHTML = var_Error;

			fetch (var_URL_API).then(function(response)
			{
				console.log(response);
			    if (response.ok)
			    {
			      response.json().then(function(data)
			      {

			        console.log(data);
			        console.log(data.competitiveStats);

			        let var_src_Img_Profil = data.icon;
			        let varPlayed = data.quickPlayStats.games.played;
			        let varPlayed2 = data.competitiveStats.games.won;
			        let varWon = data.quickPlayStats.games.won;
			        let varWon2 = data.competitiveStats.games.won;
			        let varRating = data.rating;

			        affichage_Image_Profil.setAttribute("src", data.icon);

			        console.log(`Parties jouées : ${ varPlayed}`);
			        affichage_Played.innerHTML = `Parties jouées : ${ varPlayed}`;

			        console.log(`classement : ${ varRating}`);
			        affichage_Classement.innerHTML = `classement : ${ varRating}`;

			        console.log(`Nombre de défaites : ${ varPlayed-varWon}`);
			        affichage_Deff.innerHTML = `Nombre de défaites : ${ (varPlayed-varWon)+(varPlayed2-varWon2)}`;

			        console.log(`Nombre de victoires : ${ varWon}`);
			        affichage_Victoire.innerHTML = `Nombre de victoires : ${ varWon+varWon2}`;

			        reset_Fields(this);

			      })
			    }
			    else
			    {
			    	var_Error = "Erreur, le battletag ou la plateforme de jeu est incorrect.";
			    	var_Error_Affichage.innerHTML = var_Error;
			    }
			});
		}
		else
		{
			var_Error = "Erreur, veuillez rentrer un prénom valide.";
			var_Error_Affichage.innerHTML = var_Error;
		}
	}
	else
	{
		var_Error = "Erreur, veuillez rentrer un nom valide.";
		var_Error_Affichage.innerHTML = var_Error;
	}

});
